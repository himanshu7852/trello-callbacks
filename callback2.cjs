/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const path = require("path");

const lists= path.resolve("../lists.json");

const fs = require('fs');

function getListBelongToBoard(boardId, callback){
    setTimeout(()=>{
        fs.readFile(lists,'utf-8',(error, data)=>{
            if(error){
                console.log('Error'+error);
            }else{
                const getListData = JSON.parse(data);
                callback(null, getListData[boardId])
            }
        })
    },2000)
}
module.exports = getListBelongToBoard;
