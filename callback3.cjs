/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const path = require("path");

const cards = path.resolve("../cards.json");

const fs = require('fs');

function getCardsBelongsToList(listId, callback){
    setTimeout(()=>{
        fs.readFile(cards,'utf-8',(error, data)=>{
            if(error){
                console.error('Errro:'+ error);
            }else{
                const getCardsFromListID = JSON.parse(data);
                callback(null, getCardsFromListID[listId])
            }
        })
    },2000)
}
module.exports = getCardsBelongsToList;
