/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/
const path=require('path');
const board=path.resolve('../boards.json');

const fs = require('fs');

function boardsInfoFromID(boardId, callback){
    setTimeout(() => {
        fs.readFile(board,'utf-8', (error, data)=>{
            if(error){
				console.log('Error:'+ error)
                //console.log(error);
            }else{
                const boardInfo=(JSON.parse(data));
				
				const findBoardInfoByID = boardInfo.filter((boardID)=>{
                return boardID.id === boardId;
              })

               callback(null, findBoardInfoByID);
            }
        })
        
    },2000);
}
module.exports= boardsInfoFromID;
