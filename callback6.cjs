/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const path = require('path');
const fs = require('fs');

const thanosBoardInfo= require('./callback1.cjs');
const thanosBoardList = require('./callback2.cjs');
const cardListThanos = require('./callback3.cjs');

function getDetailsUsingInfo(Thanos){

    setTimeout(()=>{

        thanosBoardInfo(Thanos,(error, data)=>{
            if(error)
            {
                console.error('Error:'+error);
            }
            else{
                console.log(data);
                thanosBoardList(Thanos, (error, data)=>{

                    if(error)
                    {
                        console.error('Error:'+error);
                    }
                    else{
                        console.log(data);
                        const listOfId = data.map((stone)=>{
                            cardListThanos(stone.id,((err,data)=>{
                                if(err)
                                {
                                    console.log(err)
                                }
                                else{
                                    if(data!=undefined)
                                    {
                                        console.log(data)
                                    }
                                }
                            }))

                            
                        })
                }
            })
         }
     })
 
   },2000)
 
 }
 module.exports= getDetailsUsingInfo;